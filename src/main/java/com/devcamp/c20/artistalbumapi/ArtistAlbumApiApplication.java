package com.devcamp.c20.artistalbumapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArtistAlbumApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtistAlbumApiApplication.class, args);
	}

}
