package com.devcamp.c20.artistalbumapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.devcamp.c20.artistalbumapi.models.Album;

@Service
public class AlbumService {

    public ArrayList<String> createSongs1() {
        ArrayList<String> songs1 = new ArrayList<>();
        songs1.add("bai hat 1");
        songs1.add("bai hat 2");
        songs1.add("bai hat 3");
        return songs1;
    }

    public ArrayList<String> createSongs2() {
        ArrayList<String> songs2 = new ArrayList<>();
        // Thêm các tên bài hát vào danh sách songs2
        songs2.add("ten bai hat 1");
        songs2.add("ten bai hat 2");
        songs2.add("ten bai hat 3");
        return songs2;
    }

    public ArrayList<String> createSongs3() {
        ArrayList<String> songs3 = new ArrayList<>();
        // Thêm các tên bài hát vào danh sách songs3
        songs3.add("ten bai hat 1");
        songs3.add("ten bai hat 2");
        songs3.add("ten bai hat 3");
        return songs3;
    }

    public ArrayList<String> createSongs4() {
        ArrayList<String> songs1 = new ArrayList<>();
        songs1.add("Crazy in Love");
        songs1.add("Single Ladies");
        songs1.add("Formation");
        return songs1;
    }

    public ArrayList<String> createSongs5() {
        ArrayList<String> songs2 = new ArrayList<>();
        // Thêm các tên bài hát vào danh sách songs2
        songs2.add("Thinking Out Loud");
        songs2.add("Shape of You");
        songs2.add("Perfect");
        return songs2;
    }

    public ArrayList<String> createSongs6() {
        ArrayList<String> songs3 = new ArrayList<>();
        // Thêm các tên bài hát vào danh sách songs3
        songs3.add("Someone Like You");
        songs3.add("Rolling in the Deep");
        songs3.add("Hello");
        return songs3;
    }

    Album myWorld = new Album(1, "My World 2.0", createSongs1());
    Album purpose = new Album(2, "Purpose", createSongs1());
    Album Changes = new Album(3, "Changes", createSongs1());

    Album vutMat = new Album(4, "Vut Mat", createSongs2());
    Album chayNgayDi = new Album(5, "Chay Ngay Di", createSongs2());
    Album tamTrang = new Album(6, "Tam Trang", createSongs2());

    Album ngayTroVe = new Album(7, "Ngay Tro Ve", createSongs3());
    Album tinhThoiXotXa = new Album(8, "Tinh Thoi Xot Xa", createSongs3());
    Album tinhKhucGoiTenEm = new Album(9, "Tinh Khuc Goi Ten Em", createSongs3());

    Album album1 = new Album(1, "Album1", createSongs4());
    Album album2 = new Album(2, "Album2", createSongs4());
    Album album3 = new Album(3, "Album3", createSongs4());

    Album album4 = new Album(4, "Album4", createSongs5());
    Album album5 = new Album(5, "Album5", createSongs5());
    Album album6 = new Album(6, "Album6", createSongs5());

    Album album7 = new Album(7, "Album7", createSongs6());
    Album album8 = new Album(8, "Album8", createSongs6());
    Album album9 = new Album(9, "Album9", createSongs6());

    public ArrayList<Album> getAlbum1(){
        ArrayList<Album> album1 = new ArrayList<>();

        album1.add(myWorld);
        album1.add(purpose);
        album1.add(Changes);

        return album1;
    }

    public ArrayList<Album> getAlbum2(){
        ArrayList<Album> album2 = new ArrayList<>();

        album2.add(vutMat);
        album2.add(chayNgayDi);
        album2.add(tamTrang);

        return album2;
    }

    public ArrayList<Album> getAlbum3(){
        ArrayList<Album> album3 = new ArrayList<>();

        album3.add(ngayTroVe);
        album3.add(tinhThoiXotXa);
        album3.add(tinhKhucGoiTenEm);

        return album3;
    }

    public ArrayList<Album> getAlbum4(){
        ArrayList<Album> album4 = new ArrayList<>();

        album4.add(album1);
        album4.add(album2);
        album4.add(album3);

        return album4;
    }

    public ArrayList<Album> getAlbum5(){
        ArrayList<Album> albumz = new ArrayList<>();

        albumz.add(album4);
        albumz.add(album5);
        albumz.add(album6);

        return albumz;
    }

    public ArrayList<Album> getAlbum6(){
        ArrayList<Album> albumF = new ArrayList<>();

        albumF.add(album7);
        albumF.add(album8);
        albumF.add(album9);

        return albumF;
    }

    public ArrayList<Album> getAllAlbum(){
        ArrayList<Album> allAlbum = new ArrayList<>();

        allAlbum.add(myWorld);
        allAlbum.add(purpose);
        allAlbum.add(Changes);
        allAlbum.add(vutMat);
        allAlbum.add(chayNgayDi);
        allAlbum.add(tamTrang);
        allAlbum.add(ngayTroVe);
        allAlbum.add(tinhThoiXotXa);
        allAlbum.add(tinhKhucGoiTenEm);

        allAlbum.add(album1);
        allAlbum.add(album2);
        allAlbum.add(album3);
        allAlbum.add(album4);
        allAlbum.add(album5);
        allAlbum.add(album6);
        allAlbum.add(album7);
        allAlbum.add(album8);
        allAlbum.add(album8);
        

        return allAlbum;
    }

    public Album filterAlbum(@RequestParam(name="code", required = true) int id){
        ArrayList<Album> album = getAllAlbum();

        Album findAlbum = new Album();

        for (Album albumElement : album) {
            findAlbum = albumElement;
            break;
        }
        
        return findAlbum;
    }
}
