package com.devcamp.c20.artistalbumapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.devcamp.c20.artistalbumapi.models.Artist;

@Service
public class ArtistService {
    @Autowired
    AlbumService albumService;

    public ArrayList<Artist> getAllArtist() {
        ArrayList<Artist> allArtist = new ArrayList<>();

        Artist casi1 = new Artist(1, "Justin Bieber");
        Artist casi2 = new Artist(2, "Soobin Hoang Son");
        Artist casi3 = new Artist(3, "Dan Truong");

        Artist casi4 = new Artist(4, "Beyonce");
        Artist casi5 = new Artist(5, "Ed Sheeran");
        Artist casi6 = new Artist(6, "Adele");

        casi1.setAlbums(albumService.getAlbum1());
        casi2.setAlbums(albumService.getAlbum2());
        casi3.setAlbums(albumService.getAlbum3());

        casi4.setAlbums(albumService.getAlbum4());
        casi5.setAlbums(albumService.getAlbum5());
        casi6.setAlbums(albumService.getAlbum6());


        allArtist.add(casi1);
        allArtist.add(casi2);
        allArtist.add(casi3);

        allArtist.add(casi4);
        allArtist.add(casi5);
        allArtist.add(casi6);

        return allArtist;
    }

    public Artist getArtistInfo(@RequestParam(name = "code", required = true) int id) {
        ArrayList<Artist> allArrtist = getAllArtist();

        Artist findArtist = new Artist();

        for (Artist artistElement : allArrtist) {
            if (artistElement.getId() == id) {
                findArtist = artistElement;
                break;
            }
        }
        return findArtist;
    }

    public Artist getArtistByIndex(int index) {
        ArrayList<Artist> allArtist = getAllArtist();

        if(index >= 0 && index <= allArtist.size()){
            return allArtist.get(index);
        }else{
            return null;
        }
    }
}
